<?php

namespace Narisok\DeliveryMethods;

use Illuminate\Support\ServiceProvider;

class DeliveryMethodProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(DeliveryService::class, function ($app) {
            return new DeliveryService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../commands/UpdateDeliveryMethod.php' => app_path('Console/Commands/UpdateDeliveryMethod.php')
        ], 'command');

        $this->publishes([
            __DIR__.'/../migrations/' => database_path('migrations')
        ], 'migrations');
    }
}
