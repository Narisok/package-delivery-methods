<?php

namespace Narisok\DeliveryMethods\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryCity extends Model
{
    use HasFactory;

    protected $table = 'delivery_cities';

    protected $fillable = [
        'method',
        'ref',
    ];

    protected $appends = [
        'title',
    ];

    public function warehouses()
    {
        return $this->hasMany(DeliveryWarehouse::class, 'delivery_city_ref', 'ref');
    }

    public function translations()
    {
        return $this->hasMany(DeliveryCityTranslation::class, 'delivery_city_id', 'id');
    }

    public function translation()
    {
        $supportedLocales = ['ru', 'uk'];
        $currentLocale = app()->currentLocale();
        return $this->hasOne(DeliveryCityTranslation::class, 'delivery_city_id', 'id')
            ->where('locale', '=', in_array($currentLocale, $supportedLocales) ? $currentLocale : 'uk');
    }


    // attributes

    public function getTitleAttribute()
    {
        return $this->translation->title;
    }
}
