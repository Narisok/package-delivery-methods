<?php

namespace Narisok\DeliveryMethods\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Narisok\DeliveryMethods\Models\DeliveryWarehouse as ModelsDeliveryWarehouse;

class DeliveryWarehouseTranslation extends Model
{
    use HasFactory;

    protected $table = 'delivery_warehouse_translations';

    protected $fillable = [
        'delivery_warehouse_id',
        'locale',
        'title',
    ];

    public function warehouse()
    {
        return $this->belongsTo(ModelsDeliveryWarehouse::class);
    }
}
