<?php

namespace Narisok\DeliveryMethods\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryCityTranslation extends Model
{
    use HasFactory;

    protected $table = 'delivery_city_translations';

    protected $fillable = [
        'delivery_city_id',
        'locale',
        'title',
    ];

    public function city()
    {
        return $this->belongsTo(DeliveryCity::class);
    }
}
