<?php

namespace Narisok\DeliveryMethods\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryWarehouse extends Model
{
    use HasFactory;

    protected $table = 'delivery_warehouses';

    protected $fillable = [
        'delivery_city_ref',
        'method',
        'ref',
        'number',
    ];

    protected $appends = [
        'title',
    ];

    public function city()
    {
        return $this->belongsTo(DeliveryCity::class, 'delivery_city_ref', 'ref');
    }

    public function translations()
    {
        return $this->hasMany(DeliveryWarehouseTranslation::class, 'delivery_warehouse_id', 'id');
    }

    public function translation()
    {
        $supportedLocales = ['ru', 'uk'];
        $currentLocale = app()->currentLocale();
        return $this->hasOne(DeliveryWarehouseTranslation::class, 'delivery_warehouse_id', 'id')
            ->where('locale', '=', in_array($currentLocale, $supportedLocales) ? $currentLocale : 'uk');
    }

    // attributes

    public function getTitleAttribute()
    {
        return $this->translation->title;
    }
}
