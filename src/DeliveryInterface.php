<?php

namespace Narisok\DeliveryMethods;

interface DeliveryInterface
{
    public function getName();
    public function update();
}

