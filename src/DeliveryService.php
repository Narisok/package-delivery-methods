<?php

namespace Narisok\DeliveryMethods;

use Narisok\DeliveryMethods\Models\DeliveryCity;
use Narisok\DeliveryMethods\Models\DeliveryWarehouse;

class DeliveryService
{
    private $methods;

    public function __construct()
    {
        $this->methods = collect();
    }

    public function availableMethods()
    {
        return $this->methods->keys();
    }

    public function registerMethod(DeliveryInterface $method)
    {
        $this->methods->put($method->getName(), $method);
    }

    public function updateMethods()
    {
        foreach($this->methods as $method) {
            $method->update();
        }
    }

    public function updateMethod($method)
    {
        $method->update();
    }


    public function findCity($method, $city_name)
    {
        $cityName = addslashes($city_name);
        return DeliveryCity::where('method', '=', $method)
            ->whereHas('translations', function($q) use($cityName) {
                $q->whereRaw('UPPER(title) LIKE \'%'.mb_strtoupper($cityName, 'UTF-8').'%\'');
            })
        // ->orderByRaw(
        //     'CASE
        //         WHEN UPPER(`title`) = \''.mb_strtoupper($cityName, 'UTF-8').'\'
        //         THEN 1
        //         WHEN UPPER(`title`) LIKE \''.mb_strtoupper($cityName, 'UTF-8').'%\'
        //         THEN 2
        //         WHEN UPPER(`title`) LIKE \'%'.mb_strtoupper($cityName, 'UTF-8').'%\'
        //         THEN 3
        //     END
        //     '
        // )
            ->with(['translation'])
            ->get()
            ->sortBy(function($q)use($cityName) {
                $len = strlen($q->title);
                if( $len == strlen($cityName)) {
                    return 1;
                } else if($q->title && $cityName && strpos($q->title, $cityName) === 0) {
                    return 2000 + $len;
                }
                return 3000 + $len;
            })
            ->take(10);
    }

    public function findWarehouse($method, $cityRef, $warehouse_number)
    {
        $warehouseNumber = addslashes($warehouse_number);
        return DeliveryWarehouse::where('method', '=', $method)
            ->where('delivery_city_ref', $cityRef)
            ->where('number', 'like', '%'.$warehouse_number.'%')
            ->orderByRaw(
                'CASE
                    WHEN UPPER(`number`) = \''.mb_strtoupper($warehouseNumber, 'UTF-8').'\'
                    THEN 1
                    WHEN UPPER(`number`) LIKE \''.mb_strtoupper($warehouseNumber, 'UTF-8').'%\'
                    THEN 2
                    WHEN UPPER(`number`) LIKE \'%'.mb_strtoupper($warehouseNumber, 'UTF-8').'%\'
                    THEN 3
                END
                '
            )
            ->limit(10)
            ->get();
    }

}

