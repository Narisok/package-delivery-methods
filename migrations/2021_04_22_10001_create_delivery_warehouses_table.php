<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_warehouses', function (Blueprint $table) {
            $table->id();

            $table->string('delivery_city_ref', 50);

            $table->string('method', 100)->index();
            $table->string('ref', 50)->index();
            $table->string('number', 50)->index();

            $table->timestamps();

            $table->foreign('delivery_city_ref')->references('ref')->on('delivery_cities')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_warehouses');
    }
}
