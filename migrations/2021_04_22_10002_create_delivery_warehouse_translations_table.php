<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryWarehouseTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_warehouse_translations', function (Blueprint $table) {
            $table->id();

            $table->foreignId('delivery_warehouse_id')->constrained('delivery_warehouses')->onDelete('CASCADE');

            $table->string('locale', 10)->index();
            $table->string('title', 190)->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_cities');
    }
}
